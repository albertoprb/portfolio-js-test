var _ = require('underscore');

var tokenizer = function(text){
	return text.match(/[a-zA-Z]+/g);
};

var toLowerCase = function(words){
	var lowered = [];
	_.each(words, function(word){
		lowered.push(word.toLowerCase());
	});
	return lowered;
};

var countWordInTokenizedText = function(word, tokenizedText){
	var matches = 0;
	_.each(tokenizedText, function(token){
		if(token === word) matches++;
	});
	return matches;
};

var frequencyDistributionParser = function(text1, text2, completeText){
	var wordCount = [];
	_.each(completeText, function(word){
		var counted = [];
		counted.push(word);
		counted.push(countWordInTokenizedText(word, text1));
		counted.push(countWordInTokenizedText(word, text2));
		counted.push(countWordInTokenizedText(word, completeText));
		wordCount.push(counted);
	});
	return _.uniq(wordCount);
};

var csvCounterBuilder = function(text1, text2, sort){
	var text1Analised = toLowerCase(tokenizer(text1));
	var text2Analised = toLowerCase(tokenizer(text2));
	var completeText = toLowerCase(tokenizer(text1 + " " + text2));

	var csvHead = "word, frequency_doc1, frequency_doc2, frequency_total";
	var csvBody = frequencyDistributionParser(text1Analised, text2Analised, completeText);

	if(sort === 'sortAZ'){
		csvBody = _.sortBy(csvBody, function(row){ return row[0]; })
	}

	if(sort === 'sortLength'){
		csvBody = _.sortBy(csvBody, function(row){ return row[0].length; })
	}

	if(sort === 'sortFreq'){
		csvBody = _.sortBy(csvBody, function(row){ return -row[3]; })
	}

	var csv = [csvHead];
	_.each(csvBody, function(row){
		csv.push(row[0] + "," + row[1] + "," + row[2] + "," + row[3]);
	});
	return _.uniq(csv);
};

exports.index = function(req, res) {
	res.render('form');
};

exports.csv = function(req, res) {
	res.send(csvCounterBuilder(req.body.text1, req.body.text2, req.body.sort));
};

exports.chart = function(req, res) {
	var csv = csvCounterBuilder(req.body.text1, req.body.text2, "sortFreq");
	var csvBody = _.last(csv, csv.length - 1);
	csvBody = _.first(csvBody, 10);

	var categories = [];
	var values = [];
	_.each(csvBody, function(row){
		var rowArray = row.split(",");
		categories.push(rowArray[0]);
		values.push(rowArray[3]);
	});
    res.render('result', {categories:categories, values: values});
};
