var path = require('path'),
rootPath = path.normalize(__dirname + '/../../..');

module.exports = {
  root: rootPath,
  cookie_secret: '123=*=456',
  port: 3000
};
