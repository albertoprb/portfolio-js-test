var config = require('./config');

module.exports = function(app) {
	app.listen(config.port);
	console.log('App started on port ' + config.port);
};