config = require('./config');

module.exports = function(app) {
    // Auth
    var tokenizer = require('../controllers/tokenizer');
	app.get('/', tokenizer.index);
	app.post('/csv', tokenizer.csv);
	app.post('/chart', tokenizer.chart);

};