/**
 * Module dependencies.
 */
var express = require('express'),
	path = require('path'),
	config = require('./config');

module.exports = function(app) {
	app.set('showStackError', true);

	//Should be placed before express.static
	app.use(express.compress({
		filter: function(req, res) {
			return (/json|text|javascript|css/).test(res.getHeader('Content-Type'));
		},
		level: 9
	}));

	//Setting the fav icon and static folder
	app.use(express.favicon());
	app.use(express.static(path.join(__dirname, '../public')));

	//Set views path, template engine and default layout
	app.set('views', path.join(__dirname, '../views'));
	app.set('view engine', 'ejs');

	//Don't use logger for test env
	if(process.env.NODE_ENV !== 'test') {
		app.use(express.logger('dev'));
	}

	//Enable jsonp
	app.enable('jsonp callback');

	app.configure(function() {

		//cookieParser should be above session
		app.use(express.cookieParser());
		app.use(express.session({secret: config.cookie_secret}));

		//bodyParser should be above methodOverride
		app.use(express.bodyParser());
		app.use(express.methodOverride());

		//routes should be at the last
		app.use(app.router);

	});
};