/**
 * Module dependencies.
 */
var express = require('express');

// Load configurations. If test env, load example file
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var app = express();

// Express settings
require('./config/express')(app);

// Bootstrap routes
require('./config/routes')(app);

// Start the app
require('./config/app')(app);

// Expose app
exports = module.exports = app;